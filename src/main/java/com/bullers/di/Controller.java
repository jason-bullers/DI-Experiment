package com.bullers.di;

import com.bullers.di.manager.Manager;
import javax.inject.Inject;

/**
 * A {@code Controller} is...
 */
public class Controller {

    @Inject Manager manager;

    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }
}
