package com.bullers.di;

/**
 * A {@code Main} is...
 */
public class Main {

    public static void main(String[] args) {
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model, view);
        MyComponent injector = DaggerMyComponent.builder()
                                                .controller(controller)
                                                .model(model)
                                                .view(view)
                                                .build();
        injector.injectController(controller);
        injector.injectModel(model);
        injector.injectView(view);

        view.getMenuBar().doSomething();
    }
}
