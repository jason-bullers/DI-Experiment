package com.bullers.di;

import com.bullers.di.actions.BaseAction;
import java.util.ArrayList;
import java.util.List;

/**
 * A {@code MenuBar} is...
 */
public class MenuBar {

    private final List<BaseAction> actions;

    public MenuBar(List<BaseAction> actions) {
        this.actions = new ArrayList<>(actions);
    }

    public void doSomething() {
        actions.forEach(a -> a.actionPerformed(null));
    }
}
