package com.bullers.di;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * A {@code MenuBarActions} is...
 */
@Qualifier
@Documented
@Retention(RUNTIME)
public @interface MenuBarActions {
}
