package com.bullers.di;

/**
 * A {@code Model} is...
 */
public class Model {

    public void doSomething() {
        System.out.println("Doing something...");
    }

    public void doSomethingElse() {
        System.out.println("Doing another thing...");
    }
}
