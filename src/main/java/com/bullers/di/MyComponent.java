package com.bullers.di;

import com.bullers.di.actions.ActionModule;
import dagger.BindsInstance;
import dagger.Component;

/**
 * A {@code MyComponent} is...
 */
@Component(modules = { MyModule.class, ActionModule.class })
interface MyComponent {

    void injectController(Controller controller);

    void injectModel(Model model);

    void injectView(View view);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder controller(Controller controller);

        @BindsInstance
        Builder model(Model model);

        @BindsInstance
        Builder view(View view);

        MyComponent build();
    }
}
