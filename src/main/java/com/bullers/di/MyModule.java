package com.bullers.di;

import com.bullers.di.actions.BaseAction;
import dagger.Module;
import dagger.Provides;
import java.util.List;

/**
 * A {@code MyModule} is...
 */
@Module
class MyModule {

    @Provides
    MenuBar provideMenuBar(@MenuBarActions List<BaseAction> actions) {
        return new MenuBar(actions);
    }
}
