package com.bullers.di;

import javax.inject.Inject;

/**
 * A {@code View} is...
 */
public class View {

    private MenuBar menuBar;

    @Inject
    public void setMenuBar(MenuBar menuBar) {
        this.menuBar = menuBar;
    }

    public MenuBar getMenuBar() {
        return menuBar;
    }
}
