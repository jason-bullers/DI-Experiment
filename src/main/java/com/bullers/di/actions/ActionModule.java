package com.bullers.di.actions;

import com.bullers.di.MenuBarActions;
import com.bullers.di.Model;
import dagger.Module;
import dagger.Provides;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * A {@code ActionModule} is...
 */
@Module
public class ActionModule {

    @Provides
    MyAction provideMyAction(Model model) {
        MyAction action = new MyAction();
        action.setModel(model);
        return action;
    }

    @Provides
    @MenuBarActions
    List<BaseAction> provideActions(MyAction action1, MyOtherAction action2) {
        return asList(action1, action2);
    }

    @Provides
    List<BaseAction> provideOtherActions(MyAction action1) {
        return singletonList(action1);
    }
}
