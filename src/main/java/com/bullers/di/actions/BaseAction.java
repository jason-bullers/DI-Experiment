package com.bullers.di.actions;

import com.bullers.di.Model;
import javax.inject.Inject;
import javax.swing.AbstractAction;

/**
 * A {@code BaseAction} is...
 */
public abstract class BaseAction extends AbstractAction {

    private Model model;

    @Inject
    public void setModel(Model model) {
        this.model = model;
    }

    protected Model getModel() {
        return model;
    }
}
