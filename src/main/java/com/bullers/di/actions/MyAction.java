package com.bullers.di.actions;

import java.awt.event.ActionEvent;
import javax.inject.Inject;

/**
 * A {@code MyAction} is...
 */
public class MyAction extends BaseAction {

    public void actionPerformed(ActionEvent e) {
        getModel().doSomething();
    }
}
