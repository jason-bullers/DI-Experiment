package com.bullers.di.actions;

import java.awt.event.ActionEvent;
import javax.inject.Inject;

/**
 * A {@code MyOtherAction} is...
 */
public class MyOtherAction extends BaseAction {

    @Inject
    MyOtherAction() {}

    public void actionPerformed(ActionEvent e) {
        getModel().doSomethingElse();
    }
}
