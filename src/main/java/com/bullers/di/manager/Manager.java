package com.bullers.di.manager;

import com.bullers.di.Controller;
import javax.inject.Inject;

/**
 * A {@code Manager} is...
 */
public class Manager {

    private final Controller controller;

    @Inject
    public Manager(Controller controller) {
        this.controller = controller;
    }
}
